import React from 'react';
import { Container } from 'reactstrap';
import './App.css';
import Search from './components/search';

function App() {


  return (
    <Container>
      <h1>ASTRON Data Portal</h1>
      <Search />
    </Container>
  );
}

export default App;
