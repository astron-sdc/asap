import React from 'react';
import { Button, Card, CardBody, CardTitle, CardText } from 'reactstrap';

export default class Observation extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			viewDetail: false
		};
	}

	displayDetail = () => {
		this.setState({
			viewDetail: !this.state.viewDetail
		});
	}

	render() {
		return (
			<div>
				<Button onClick={this.displayDetail} color="link">
					{this.props.observation.obsName}
				</Button>
				{this.state.viewDetail ? (
					<Card>
						<CardBody>
							<CardTitle>
								source: {this.props.observation.source}
							</CardTitle>
							<CardText>
								Description of the observation
							</CardText>
						</CardBody>
					</Card>
				) : null}
			</div>
		);
	}
}