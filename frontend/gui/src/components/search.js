import React from 'react';
import axios from 'axios';
import { InputGroup, Input, Button } from 'reactstrap';
import Observations from './observations';

export default class Search extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
      searchString: '',
      observations: []};
	}

	handleChange = (event) => {
    this.setState({
      searchString: event.target.value
    })
  }

  handleSubmit = (event) => {
    // console.log(this.state.searchString);
    axios.get('http://127.0.0.1:8000/api/observations',{
      params: {
        source: this.state.searchString
      }
    })
    .then(res => {
      this.setState({
        observations: res.data
      });
    });
  }

	render() {
    // console.log(this.state.observations);
		return (
			<div>
				<InputGroup>
          <Input
            type="search"
            placeholder="e.g. 3C48"
            value={this.state.searchString}
            onChange={this.handleChange}
          />
          <Button onClick={this.handleSubmit}>Search</Button>
        </InputGroup>
        <Observations observations={this.state.observations} />
	    </div>
		);
	}
}