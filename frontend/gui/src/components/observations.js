import React from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';
import Observation from './observation';

export default function Observations(props) {
	const observations = props.observations;
	return (
		<ListGroup>
			{observations.map(
				(observation) => 
					<ListGroupItem key={observation.id}>
						<Observation observation={observation} />
					</ListGroupItem>
			)}
    </ListGroup>
	);
} 
