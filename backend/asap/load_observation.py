#!/usr/bin/env python3

import json
from dataportal.models import Observation


with open('wsrt_metadata.json') as f:
  obs_json = json.load(f)

for obs in obs_json:
  obs = Observation(obsName=obs['obsName'], source=obs['source'])
  obs.save()



