from django.urls import path, include
from rest_framework import routers
from .views import ObservationView

router = routers.DefaultRouter()					# add this
router.register(r'observations', ObservationView, 'observation')		# add this

urlpatterns = [
    path('api/', include(router.urls))		# add this
]