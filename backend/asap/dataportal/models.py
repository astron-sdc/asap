from django.db import models

# Create your models here.


class Observation(models.Model):
    """ Observation model with fields:
        obsName (observation name), source, obsDate (observation date)
    """
    obsName = models.CharField(max_length=20)
    source = models.CharField(max_length=20)
    #obsDate = models.DateTimeField()

    def __str__(self):
        return self.obsName
