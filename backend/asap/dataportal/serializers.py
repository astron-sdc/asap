from rest_framework import serializers
from dataportal.models import Observation

class ObservationSerializer(serializers.ModelSerializer):
	class Meta:
		model = Observation
		fields = ('id', 'obsName', 'source')