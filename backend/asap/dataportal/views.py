from rest_framework import viewsets
from dataportal.models import Observation
from .serializers import ObservationSerializer

class ObservationView(viewsets.ModelViewSet):
	queryset = Observation.objects.all()
	serializer_class = ObservationSerializer
	filterset_fields = ['id', 'obsName', 'source']
